﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WorkSummaryStrapiDemo.Extensions;
using WorkSummaryStrapiDemo.Models;
using WorkSummaryStrapiDemo.Services;

namespace WorkSummaryStrapiDemo.Pages
{
    public class AddSummaryModel : PageModel
    {
        private readonly IStrapiService _strapiService;
        private readonly IRedirectionService _redirectionService;

        public AddSummaryModel(IStrapiService strapiService,
            IRedirectionService redirectionService)
        {
            _strapiService = strapiService;
            _redirectionService = redirectionService;
        }

        public AuthCookie Auth { get; set; }
        public string TitleLabel { get; set; }
        public string ContentLabel { get; set; }
        public string SubmitLabel { get; set; }
        public string SummaryId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public async Task<IActionResult> OnGetAsync(string user, string summary)
        {
            var isLogin = await Request.HasLogin(user);
            if (!isLogin)
                return Redirect("Singin");
            Auth = await _strapiService.GetAuthFromPage(Request, user);
            SummaryId = summary;
            if(!string.IsNullOrEmpty(SummaryId))
            {
                var found = await _strapiService.GetSummary(Auth, SummaryId);
                if(found!=null)
                {
                    Title = found.Title;
                    Content = found.Content;
                }
            }
            TitleLabel = await _strapiService.GetLabelAsync("label-title");
            ContentLabel = await _strapiService.GetLabelAsync("label-content");
            SubmitLabel = await _strapiService.GetLabelAsync("label-submit");
            return Page();
        }

        public async Task<ActionResult> OnPostAddSummary(string summaryid, string userid)
        {
            var auth = await _strapiService.GetAuthFromPage(Request, userid);
            var title = Request.Form["title"];
            var content = Request.Form["content"];
            var user = auth.User;
            var created = DateTime.Now;
            var updated = DateTime.Now;
            if (string.IsNullOrEmpty(summaryid))
            {
                await _strapiService.AddSummary(auth,
                    new WorkSummary
                    {
                        Title = title,
                        Content = content,
                        Account = new User { Id = user },
                        Created = created,
                        LastUpdated = updated
                    });
            }
            else 
            {
                await _strapiService.UpdateSummary(auth,
                    new WorkSummary
                    {
                        Id= summaryid,
                        Title = title,
                        Content = content,
                        Account = new User { Id = user },
                        LastUpdated = updated
                    });
            }

            return Redirect(_redirectionService.GetRedirectionUrl($"Index?user={auth.User}"));
        }
    }
}