﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WorkSummaryStrapiDemo.Models;
using WorkSummaryStrapiDemo.Services;

namespace WorkSummaryStrapiDemo.Pages
{
    public class SigninModel : PageModel
    {
        private readonly IStrapiService _strapiService;
        private readonly IRedirectionService _redirectionService;

        public SigninModel(
            IStrapiService strapiService,
            IRedirectionService redirectionService)
        {
            _strapiService = strapiService;
            _redirectionService = redirectionService;
        }

        public string UserNameLabel { get; set; }
        public string PasswordLabel { get; set; }
        public string SignupLabel { get; set; }
        public string SigninLabel { get; set; }
        public string RememberMeLabel { get; set; }
        public string NoSuchUserLabel { get; set; }
        public string Message { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            UserNameLabel = await _strapiService.GetLabelAsync("label-username");
            PasswordLabel = await _strapiService.GetLabelAsync("label-password");
            SignupLabel = await _strapiService.GetLabelAsync("label-signup");
            SigninLabel = await _strapiService.GetLabelAsync("label-signin");
            RememberMeLabel = await _strapiService.GetLabelAsync("label-rememberme");
            NoSuchUserLabel = await _strapiService.GetLabelAsync("label-no-such-user");

            return Page();
        }

        public async Task<ActionResult> OnPostSignin()
        {
            var username = Request.Form["username"];
            var password = Request.Form["password"];
            var remember = Request.Form["remember"] == "on";
            var user = await _strapiService.FindUserAsync(username, password);
            var succeed = false;
            Message = NoSuchUserLabel;

            if (user != null)
            {
                if (remember)
                {
                    var authorization = await _strapiService.GetAuthorizationAsync();
                    if (authorization != null)
                    {
                        var authCookie = new AuthCookie { User = user.Id, Jwt = authorization.Jwt };
                        Response.SetAuthenticationCookie(authCookie);
                        succeed = true;
                    }
                }
                else 
                {
                    succeed = true;
                }
            }
            
            if(succeed)
            {
                return Redirect(_redirectionService.GetRedirectionUrl($"Index?user={user.Id}"));
            }
            else 
            {
                return Redirect(_redirectionService.GetRedirectionUrl("Signin"));
            }
        }

        public async Task<ActionResult> OnPostSignup()
        {
            return await Task.FromResult(Redirect(_redirectionService.GetRedirectionUrl("Signup")));
        }
    }
}