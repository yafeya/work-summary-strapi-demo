﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using WorkSummaryStrapiDemo.Extensions;
using WorkSummaryStrapiDemo.Models;
using WorkSummaryStrapiDemo.Services;

namespace WorkSummaryStrapiDemo.Pages
{
    public class IndexModel : PageModel
    {
        private const int PageSize = 10;

        private readonly ILogger<IndexModel> _logger;
        private readonly IStrapiService _strapiService;
        private readonly IRedirectionService _redirectionService;

        public IndexModel(
            ILogger<IndexModel> logger, 
            IStrapiService strapiService,
            IRedirectionService redirectionService)
        {
            _logger = logger;
            _strapiService = strapiService;
            _redirectionService = redirectionService;
            DomainBaseUrl = _redirectionService.GetDomainBaseUrl();
        }

        public string DomainBaseUrl { get; set; }
        public int TotalPages { get; set; }
        public IEnumerable<WorkSummary> Summaries { get; set; }
        public string UserId { get; set; }
        public int CurrentPage { get; set; }
        public string Jwt { get; set; }
        public bool ShowPagination { get; set; }
        public string AddSummaryLabel { get; set; }
        public string PreviousPageLabel { get; set; }
        public string NextPageLabel { get; set; }

        public async Task<IActionResult> OnGetAsync(string user, int currentPage = 0)
        {
            try
            {
                var isLogin = await Request.HasLogin(user);
                if (!isLogin)
                    return Redirect(_redirectionService.GetRedirectionUrl("Signin"));
                var authCookie = await _strapiService.GetAuthFromPage(Request, user);
                UserId = authCookie.User;
                Jwt = authCookie.Jwt;
                CurrentPage = currentPage;
                var totalCount = await _strapiService.GetTotalCount(new AuthCookie { User = UserId, Jwt = Jwt });
                TotalPages = totalCount / PageSize + (totalCount % PageSize > 0 ? 1 : 0);
                Summaries = await _strapiService.GetCurrentPage(new AuthCookie { User = UserId, Jwt = Jwt }, start: CurrentPage * PageSize, limit: PageSize);
                ShowPagination = TotalPages > 1;
                AddSummaryLabel = await _strapiService.GetLabelAsync("label-add-summary");
                PreviousPageLabel = await _strapiService.GetLabelAsync("label-previous-page");
                NextPageLabel = await _strapiService.GetLabelAsync("label-next-page");
                return Page();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return Redirect(_redirectionService.GetRedirectionUrl("Error"));
            }
        }
    }
}
