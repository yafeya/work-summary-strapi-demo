﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WorkSummaryStrapiDemo.Services;

namespace WorkSummaryStrapiDemo.Pages
{
    public class SignupModel : PageModel
    {
        private readonly IStrapiService _strapiService;
        private readonly IRedirectionService _redirectionService;

        public SignupModel(IStrapiService strapiService,
            IRedirectionService redirectionService)
        {
            _strapiService = strapiService;
            _redirectionService = redirectionService;
            DomainBaseUrl = _redirectionService.GetDomainBaseUrl();
        }

        public string DomainBaseUrl { get; set; }
        public string UserNameLabel { get; set; }
        public string PasswordLabel { get; set; }
        public string ConfirmPasswordLabel { get; set; }
        public string SignupLabel { get; set; }
        public string UserExistedLabel { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            UserNameLabel = await _strapiService.GetLabelAsync("label-username");
            PasswordLabel = await _strapiService.GetLabelAsync("label-password");
            SignupLabel = await _strapiService.GetLabelAsync("label-signup");
            ConfirmPasswordLabel = await _strapiService.GetLabelAsync("label-confirmpassword");
            UserExistedLabel = await _strapiService.GetLabelAsync("label-user-existed");

            return Page();
        }

        public async Task<ActionResult> OnPostSignup()
        {
            var username = Request.Form["username"];
            var password = Request.Form["password"];

            var result = await _strapiService.AddUser(username, password);
            if(result)
            {
                return Redirect(_redirectionService.GetRedirectionUrl("Signin"));
            }
            else
            {
                return Redirect(_redirectionService.GetRedirectionUrl("Signup"));
            }
        }
    }
}