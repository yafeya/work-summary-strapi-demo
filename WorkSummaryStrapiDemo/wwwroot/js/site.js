﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.
$(document).ready(() => {
    $("#signup-wrapper input[name='username']").on("change", function (e) {
        var username = $(e.target).val();
        var domainBase = $("#signup-wrapper").data("domain-base");
        var apiUrl = domainBase + '/api/account/has-user?username=' + username;
        axios.get(apiUrl)
            .then(response => {
                var exist = response.data;
                if (exist) {
                    var message = $(e.target).data("message");
                    alert(message);
                    $("#signup-wrapper button").addClass("disabled");
                } else {
                    $("#signup-wrapper button").removeClass("disabled");
                }
            })
            .catch(error => alert(error));
    });
});