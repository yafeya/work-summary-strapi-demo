﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Linq;
using WorkSummaryStrapiDemo.Models;

public static class CookiesExtensions
{
    private const string CookiesKey = "strapi-demo";

    public static void SetAuthenticationCookie(this HttpResponse response, AuthCookie authCookie)
    {
        var json = JsonConvert.SerializeObject(authCookie, Formatting.Indented);
        var base64 = Base64Encode(json);
        var options = new CookieOptions
        {
            Expires = DateTime.Now.AddDays(30)
        };
        response.Cookies.Append(CookiesKey, base64, options);
    }

    public static AuthCookie GetAuthenticationCookie(this HttpRequest request)
    {
        var base64 = request.Cookies[CookiesKey];
        if (string.IsNullOrEmpty(base64)) 
            return null;
        var json = Base64Decode(base64);
        var authCookie = JsonConvert.DeserializeObject<AuthCookie>(json);
        return authCookie;
    }

    private static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }

    private static string Base64Decode(string base64EncodedData)
    {
        var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
        return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
    }
}
