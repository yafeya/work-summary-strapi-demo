﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkSummaryStrapiDemo.Models;
using WorkSummaryStrapiDemo.Services;

namespace WorkSummaryStrapiDemo.Extensions
{
    public static class StrapiServiceExtensions
    {
        public static async Task<bool> HasLogin(this HttpRequest request, string userId)
        {
            var authCookie = request.GetAuthenticationCookie();
            return await Task.FromResult(!string.IsNullOrEmpty(userId) || authCookie != null);
        }
        public static async Task<AuthCookie> GetAuthFromPage(this IStrapiService strapiService, HttpRequest request, string userId)
        {
            var authCookie = request.GetAuthenticationCookie();
            if (authCookie == null)
            {
                var authorization = await strapiService.GetAuthorizationAsync();
                authCookie = new AuthCookie
                {
                    User = userId,
                    Jwt = authorization.Jwt
                };
            }
            return authCookie;
        }
    }
}
