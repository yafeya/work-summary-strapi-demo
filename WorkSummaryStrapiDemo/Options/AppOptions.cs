﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkSummaryStrapiDemo.Options
{
    public class AppOptions
    {
        public string StrapiUrl { get; set; }
        public string DomainBase { get; set; }
    }
}
