﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkSummaryStrapiDemo.Models
{
    public class AuthCookie
    {
        public string User { get; set; }
        public string Jwt { get; set; }
    }
}
