﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkSummaryStrapiDemo.Models
{
    public class Label
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string Culture { get; set; }
    }
}
