﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkSummaryStrapiDemo.Options;

namespace WorkSummaryStrapiDemo.Services
{
    public interface IRedirectionService
    {
        string GetDomainBaseUrl();
        string GetRedirectionUrl(string page);
    }

    public class RedirectionService : IRedirectionService
    {
        private readonly IOptionsMonitor<AppOptions> _options;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RedirectionService(IOptionsMonitor<AppOptions> options)
        {
            _options = options;
        }

        public string GetDomainBaseUrl()
        {
            return string.IsNullOrEmpty(_options.CurrentValue.DomainBase) ? string.Empty : $"{_options.CurrentValue.DomainBase}";
        }

        public string GetRedirectionUrl(string page)
        {
            var domainBase = _options.CurrentValue.DomainBase;
            return string.IsNullOrWhiteSpace(domainBase)? page : $"{domainBase}/{page}";
        }
    }
}
