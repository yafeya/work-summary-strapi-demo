﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WorkSummaryStrapiDemo.Models;
using WorkSummaryStrapiDemo.Options;

namespace WorkSummaryStrapiDemo.Services
{
    public interface IStrapiService
    {
        Task<string> GetLabelAsync(string name, string culture = "zh");
        Task<bool> IsUserNameExistedAsync(string username);
        Task<User> FindUserAsync(string username, string password);
        Task<Authorization> GetAuthorizationAsync();
        Task<bool> AddUser(string username, string password);
        Task<int> GetTotalCount(AuthCookie auth);
        Task<IEnumerable<WorkSummary>> GetCurrentPage(AuthCookie auth, int start, int limit);
        Task<WorkSummary> AddSummary(AuthCookie auth, WorkSummary summary);
        Task<WorkSummary> UpdateSummary(AuthCookie auth, WorkSummary summary);
        Task<WorkSummary> GetSummary(AuthCookie auth, string summaryId);
    }

    public class StrapiService : IStrapiService
    {
        private readonly HttpClient _httpClient;
        private readonly IOptionsMonitor<AppOptions> _appOptions;

        public StrapiService(
            HttpClient httpClient,
            IOptionsMonitor<AppOptions> appOptions)
        {
            _httpClient = httpClient;
            _appOptions = appOptions;

            //var webProxy = new WebProxy(new Uri("http://127.0.0.1:8888"), BypassOnLocal: false);

            //var proxyHttpClientHandler = new HttpClientHandler
            //{
            //    Proxy = webProxy,
            //    UseProxy = true,
            //};

            //_httpClient = new HttpClient(proxyHttpClientHandler);
        }

        public async Task<bool> AddUser(string username, string password)
        {
            var url = $"{_appOptions.CurrentValue.StrapiUrl}/accounts";
            var body = new
            {
                username = username,
                password = password,
                email = string.Empty,
                name = username
            };
            var request = GeneratePostRequest(body);
            var response = await _httpClient.PostAsync(url, request);
            return response.StatusCode == System.Net.HttpStatusCode.OK;
        }

        public async Task<User> FindUserAsync(string username, string password)
        {
            var url = $"{_appOptions.CurrentValue.StrapiUrl}/accounts?username={username}&password={password}";
            var response = await _httpClient.GetAsync(url);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseValue = await response.Content.ReadAsStringAsync();
                var user = JsonConvert.DeserializeObject<User[]>(responseValue).FirstOrDefault();
                return user;
            }
            return null;
        }

        public async Task<Authorization> GetAuthorizationAsync()
        {
            var url = $"{_appOptions.CurrentValue.StrapiUrl}/auth/local";
            var body = new
            {
                identifier = "strapi@strapi.com",
                password = "Common&&123456"
            };
            var request = GeneratePostRequest(body);
            var response = await _httpClient.PostAsync(url, request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseValue = await response.Content.ReadAsStringAsync();
                var authorization = JsonConvert.DeserializeObject<Authorization>(responseValue);
                return authorization;
            }
            return null;
        }

        public async Task<string> GetLabelAsync(string name, string culture)
        {
            var result = string.Empty;
            var url = $"{_appOptions.CurrentValue.StrapiUrl}/labels?name={name}&culture={culture}";
            var response = await _httpClient.GetAsync(url);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseValue = await response.Content.ReadAsStringAsync();
                var label = JsonConvert.DeserializeObject<Label[]>(responseValue).FirstOrDefault();
                result = label.Content;
            }
            return result;
        }

        public async Task<bool> IsUserNameExistedAsync(string username)
        {
            var existed = false;
            var url = $"{_appOptions.CurrentValue.StrapiUrl}/accounts?username={username}";
            var response = await _httpClient.GetAsync(url);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseValue = await response.Content.ReadAsStringAsync();
                existed = JsonConvert.DeserializeObject<User[]>(responseValue).Any();
            }
            return existed;
        }

        public async Task<int> GetTotalCount(AuthCookie auth)
        {
            var result = -1;
            var url = $"{_appOptions.CurrentValue.StrapiUrl}/records/count?account={auth.User}";
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth.Jwt);
            var response = await _httpClient.GetAsync(url);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseValue = await response.Content.ReadAsStringAsync();
                result = int.TryParse(responseValue, out int count) ? count : -1;
            }
            return result;
        }

        public async Task<IEnumerable<WorkSummary>> GetCurrentPage(AuthCookie auth, int start, int limit)
        {
            var result = Enumerable.Empty<WorkSummary>();
            var url = $"{_appOptions.CurrentValue.StrapiUrl}/records?account={auth.User}&_start={start}&_limit={limit}";
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth.Jwt);
            var response = await _httpClient.GetAsync(url);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseValue = await response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<WorkSummary[]>(responseValue);
            }
            return result;
        }

        private HttpContent GeneratePostRequest(object body)
        {
            var request = new StringContent(JsonConvert.SerializeObject(body, Formatting.Indented));
            request.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return request;
        }

        public async Task<WorkSummary> AddSummary(AuthCookie auth, WorkSummary summary)
        {
            WorkSummary result = null;
            var url = $"{_appOptions.CurrentValue.StrapiUrl}/records";
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth.Jwt);
            var body = new 
            {
                title = summary.Title,
                content = summary.Content,
                account = auth.User,
                lastUpdated = summary.LastUpdated,
                created = summary.Created
            };
            var request = GeneratePostRequest(body);
            var response = await _httpClient.PostAsync(url, request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseValue = await response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<WorkSummary>(responseValue);
            }
            return result;
        }

        public async Task<WorkSummary> UpdateSummary(AuthCookie auth, WorkSummary summary)
        {
            WorkSummary result = null;
            if(!string.IsNullOrEmpty(summary.Id) )
            {
                var url = $"{_appOptions.CurrentValue.StrapiUrl}/records/{summary.Id}";
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth.Jwt);
                var body = new
                {
                    title = summary.Title,
                    content = summary.Content,
                    account = auth.User,
                    lastUpdated = summary.LastUpdated,
                    created = summary.Created
                };
                var request = GeneratePostRequest(body);
                var response = await _httpClient.PutAsync(url, request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseValue = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<WorkSummary>(responseValue);
                }
            }
            return result;
        }

        public async Task<WorkSummary> GetSummary(AuthCookie auth, string summaryId)
        {
            WorkSummary result = null;
            if (!string.IsNullOrEmpty(summaryId))
            {
                var url = $"{_appOptions.CurrentValue.StrapiUrl}/records/{summaryId}";
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth.Jwt);
                var response = await _httpClient.GetAsync(url);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseValue = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<WorkSummary>(responseValue);
                }
            }
            return result;
        }
    }
}
