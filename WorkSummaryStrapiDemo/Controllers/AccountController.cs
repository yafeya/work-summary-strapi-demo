﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WorkSummaryStrapiDemo.Services;

namespace WorkSummaryStrapiDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IStrapiService _strapiService;
        public AccountController(IStrapiService strapiService)
        {
            _strapiService = strapiService;
        }

        [HttpGet("has-user")]
        public async Task<ActionResult<bool>> HasUser([FromQuery]string username)
        {
            return await _strapiService.IsUserNameExistedAsync(username);
        }
    }
}